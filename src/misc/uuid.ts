import { randomNumber } from '~/number'

const DICT = 'useandom-26T198340PX75pxJACKVERYMINDBUSHWOLF_GQZbfghjklqvwyzrict'

/**
 * Generates a random string of a specified length.
 *
 * @param length - The length of the randomly generated string.
 * @returns The randomly generated string.
 */
export function uuid(length = 16) {
  const l = DICT.length
  let i = length
  let id = ''

  while (i--) {
    id += DICT[randomNumber(l)]
  }

  return id
}
