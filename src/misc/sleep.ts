/**
 * Halts thread execution for a specified time period.
 *
 * @param duration - The time period (in ms).
 * @returns The halting promise.
 */
export async function sleep(duration: number): Promise<void> {
  if (duration < 0) {
    console.warn('[tsu] warning: sleep durations less than 0 are set to 0.')
  }

  return new Promise((resolve) => {
    setTimeout(resolve, duration)
  })
}
