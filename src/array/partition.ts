/**
 * Splits an array into two based on a predicate function.
 *
 * @param array - The array.
 * @param filter - The predicate function.
 * @returns The tuple of values that satisfy the predicate and those that don't.
 */
export function partition<T, U = T>(
  array: readonly (T | U)[],
  filter: (current: T | U, idx: number, array: readonly (T | U)[]) => boolean
): [T[], U[]] {
  const passes: T[] = []
  const fails: U[] = []

  for (let i = 0; i < array.length; i++) {
    const current = array[i]

    if (filter(current, i, array)) {
      passes.push(current as T)
    } else {
      fails.push(current as U)
    }
  }

  return [passes, fails]
}
