/**
 * Returns all but the first item of an array.
 *
 * @param array - The array.
 * @returns The array without the first item.
 */
export function tail<T>(array: readonly T[]): T[] {
  return array.slice(1)
}
