import type { Arrayable, Nullable } from '~/types'

/**
 * Wraps a single value in an array, if not already an array.
 *
 * @param maybeArray - The value to wrap, or the existing array.
 * @returns The item wrapped in an array, or the provided array.
 */
export function toArray<T>(maybeArray: Nullable<Arrayable<T>>): T[] {
  if (!maybeArray) {
    return []
  }

  return Array.isArray(maybeArray) ? maybeArray : [maybeArray]
}
