/**
 * Splits an array into two at a specified index.
 *
 * @param array - The array.
 * @param i - The position to split at.
 * @returns The tuple of values before and after the split.
 */
export function splitArray<T>(array: readonly T[], i: number): [T[], T[]] {
  if (i <= 0) {
    return [[], [...array]]
  }

  if (i > array.length) {
    return [[...array], []]
  }

  return [array.slice(0, i), array.slice(i)]
}
