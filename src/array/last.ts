/**
 * Returns the last item of an array.
 *
 * @param array - The array.
 * @returns The last item.
 */
export function last(array: readonly []): undefined
export function last<T>(array: readonly T[]): T
export function last<T>(array: readonly T[] | readonly []): T | undefined {
  return array[array.length - 1]
}
