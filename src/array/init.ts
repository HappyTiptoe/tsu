/**
 * Returns all but the last item of an array.
 *
 * @param array - The array.
 * @returns The array without the last item.
 */
export function init<T>(array: readonly T[]): T[] {
  return array.slice(0, array.length - 1)
}
