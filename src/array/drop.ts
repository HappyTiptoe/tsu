/**
 * Removes the first `n` items of an array.
 *
 * @param array - The array.
 * @param n - The number of items to remove.
 * @returns The remaining items.
 */
export function drop<T>(array: readonly T[], n: number): T[] {
  if (n <= 0) {
    return [...array]
  }

  return array.slice(n)
}
