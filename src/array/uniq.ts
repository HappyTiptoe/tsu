/**
 * Removes duplicate primitive values from an array.
 *
 * @param array - The array.
 * @returns The array without duplicated primitive values.
 */
export function uniq<T>(array: readonly T[]): T[] {
  return Array.from(new Set(array))
}
