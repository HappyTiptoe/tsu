/**
 * Generates a range of numbers.
 *
 * @param start - The inclusive start value.
 * @param stop - The exlusive stop value.
 * @param step - The step to increment by.
 * @returns The range of numbers.
 */
export function range(stop: number): number[]
export function range(start: number, stop: number, step?: number): number[]
export function range(...args: any[]): number[] {
  let start: number
  let stop: number
  let step: number

  if (args.length === 1) {
    stop = args[0]
    start = 0
    step = 1
  } else {
    start = args[0]
    stop = args[1]
    step = args[2] || 1
  }

  if (step <= 0) {
    console.warn('[tsu] warning: a step less than or equal to 0 will be set to 1.')
    step = 1
  }

  const array: number[] = []

  let current = start

  while (current < stop) {
    array.push(current)
    current += step || 1
  }

  return array
}
