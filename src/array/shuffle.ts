import { randomNumber } from '../number'
import { clone } from '../object'

/**
 * Randomly shuffles items in an array.
 *
 * @param array - The array.
 * @returns A copy of the array with its items randomly shuffled.
 */
export function shuffle<T>(array: T[]): T[] {
  const arrayClone = clone(array)

  let i
  let j
  let x

  for (i = arrayClone.length; i; i -= 1) {
    j = randomNumber(i)
    x = arrayClone[i - 1]
    arrayClone[i - 1] = arrayClone[j]
    arrayClone[j] = x
  }

  return arrayClone
}
