/**
 * Identifies if a value is a number.
 *
 * @param val - The value.
 * @returns If the value is a number.
 */
export function isNumber(val: unknown): val is number {
  return typeof val === 'number'
}
