/**
 * Identifies if a value is undefined.
 *
 * @param val - The value.
 * @returns If the value is undefined.
 */
export function isUndefined(val: unknown): val is undefined {
  return typeof val === 'undefined'
}
