/**
 * Identifies if a value is null.
 *
 * @param val - The value.
 * @returns If the value is null.
 */
export function isNull(val: unknown): val is null {
  return Object.prototype.toString.call(val) === '[object Null]'
}
