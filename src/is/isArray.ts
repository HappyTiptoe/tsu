/**
 * Identifies if a value is an array.
 *
 * @param val - The value.
 * @returns If the value is an array.
 */
export function isArray(val: unknown): val is any[] {
  return Array.isArray(val)
}
