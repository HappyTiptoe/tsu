import { Fn } from '~/types'

/**
 * Identifies if a value is a function.
 *
 * @param val - The value.
 * @returns If the value is a function.
 */
export function isFunction<T extends Fn>(val: unknown): val is T {
  return typeof val === 'function'
}
