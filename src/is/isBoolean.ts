/**
 * Identifies if a value is a boolean.
 *
 * @param val - The value.
 * @returns If the value is a boolean.
 */
export function isBoolean(val: unknown): val is boolean {
  return typeof val === 'boolean'
}
