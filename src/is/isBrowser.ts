/**
 * Identifies if the code is being run in a browser.
 *
 * @returns If the code is being run in a browser.
 */
export function isBrowser(): boolean {
  return typeof window !== 'undefined'
}
