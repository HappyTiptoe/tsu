/**
 * Identifies if a value is an array, an object, a date, or null.
 *
 * @param val - The value.
 * @returns If the value is an array, an object, a date, or null.
 */
export function isObjectLike(val: unknown): val is object {
  return typeof val === 'object'
}
