import { isBrowser } from '../is'

/**
 * Identifies if a value is the global window.
 *
 * @param val - The value.
 * @returns If the value is the global window.
 */
export function isWindow(val: unknown): val is Window {
  return isBrowser() && Object.prototype.toString.call(val) === '[object Window]'
}
