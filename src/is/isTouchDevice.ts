/**
 * Identifies if the code is being run on a touch device.
 *
 * @returns If the code is being run on a touch device.
 */
export function isTouchDevice(): boolean {
  return 'ontouchstart' in window || navigator.maxTouchPoints > 0
}
