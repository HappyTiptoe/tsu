import { isArray } from '../is'

/**
 * Identifies if a value is an empty array.
 *
 * @param val - The value.
 * @returns If the value is an empty array.
 */
export function isEmptyArray(val: unknown): val is [] {
  return isArray(val) && val.length === 0
}
