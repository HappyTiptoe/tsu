import { Obj } from '~/types'
import { isObject } from '../is'

/**
 * Identifies if a value is an empty object.
 *
 * @param val - The value.
 * @returns If the value is an empty object.
 */
export function isEmptyObject(val: unknown): val is Obj<never> {
  if (!isObject(val)) {
    return false
  }

  // if object has any properties, it's not empty
  for (const _ in val) {
    return false
  }

  return true
}
