export type Arrayable<T> = T | Array<T>
export type Nullable<T> = T | null | undefined

export type Fn<T = any> = (...args: any[]) => T
export type VoidFn = Fn<void>

export type Obj<V = unknown> = Record<string | symbol | number, V>
