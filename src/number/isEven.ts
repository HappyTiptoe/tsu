/**
 * Identifies if a number is even.
 *
 * @param n - The number.
 * @returns If the number is even.
 */
export function isEven(n: number): boolean {
  return n % 2 === 0
}
