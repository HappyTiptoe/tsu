/**
 * Identifies if a number is odd.
 *
 * @param n - The number.
 * @returns If the number is odd.
 */
export function isOdd(n: number): boolean {
  return n % 2 !== 0
}
