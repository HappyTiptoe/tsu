/**
 * Returns the sum of a number array.
 *
 * @param ns - The number array.
 * @returns The sum.
 */
export function sum(ns: readonly number[]): number {
  let total = 0

  for (let i = 0; i < ns.length; i++) {
    total += ns[i]
  }

  return total
}
