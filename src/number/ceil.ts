/**
 * Rounds a number up to the nearest multiple of the specified factor.
 *
 * @param n - The number to round up.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
export function ceil(n: number, factor = 1) {
  const quotient = n / factor

  return Math.ceil(quotient) * factor
}
