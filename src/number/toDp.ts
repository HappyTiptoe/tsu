import { clamp } from './clamp'

/**
 * Rounds a number to a specified number of decimal places.
 *
 * @param n - The number to round.
 * @param decimalPlaces - The number of decimal places to round to.
 * @returns The rounded number.
 */
export function toDp(n: number, decimalPlaces = 0): number {
  if (decimalPlaces < 0) {
    console.warn('[tsu] warning: decimalPlaces less than 0 are set to 0.')
  }

  if (decimalPlaces > 100) {
    console.warn('[tsu] warning: decimalPlaces greater than 100 are set to 100.')
  }

  const clampedDecimalPlaces = clamp(decimalPlaces, 0, 100)

  return Number(n.toFixed(clampedDecimalPlaces))
}
