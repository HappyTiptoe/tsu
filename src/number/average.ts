import { sum } from '../number'

/**
 * Returns the average of a number array.
 *
 * @param ns - The number array.
 * @returns The average.
 */
export function average(ns: readonly number[]): number {
  if (ns.length < 1) return 0

  return sum(ns) / ns.length
}
