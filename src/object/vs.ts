import { Obj } from '~/types'

/**
 * Shorthand `Object.values`
 *
 * @param obj - The object to extract the values from.
 * @returns The values.
 */
export function vs<T>(obj: Obj<T>): T[] {
  return Object.values(obj)
}
