/**
 * Creates a shallow copy of an object, omitting the specified keys.
 *
 * @param object - The object to copy.
 * @param keysToOmit - The keys to omit from the copied object.
 * @returns A shallow copy of the object, without the specified keys.
 */
export function omit<T extends Record<string, any>, K extends keyof T>(
  object: T,
  keysToOmit: K[] | any[]
): Pick<T, Exclude<keyof T, K>> {
  const result = { ...object }

  for (const key of keysToOmit) {
    delete result[key]
  }

  return result
}
