/**
 * Strict-typed, shorthand `Object.entries`
 *
 * @param obj - The object to extract the entries from.
 * @returns The entries.
 */
export function es<T extends object>(obj: T): [keyof T, T[keyof T]][] {
  return Object.entries(obj) as [keyof T, T[keyof T]][]
}
