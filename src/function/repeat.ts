/**
 * Executes a given function a specified number of times.
 *
 * @param fn - The function to execute for each iteration.
 * @param n - The number of times to execute the function.
 */
export function repeat(fn: (i: number) => void, n: number): void {
  if (n <= 0) return

  for (let i = 0; i < n; i++) {
    fn(i)
  }
}
