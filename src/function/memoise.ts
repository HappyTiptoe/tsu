import { Fn, Obj } from '~/types'

/**
 * Optimises subsequent calls of a function by caching return values.
 *
 * @param fn - The function.
 * @returns The memoised function.
 */
export function memoise<T>(fn: Fn<T>): Fn<T> {
  const cache: Obj<T> = {}

  return function (...args) {
    const argsString = JSON.stringify(args)

    if (argsString in cache) {
      return cache[argsString]
    }

    const result = fn(...args)
    cache[argsString] = result
    return result
  }
}

export const memoize = memoise
