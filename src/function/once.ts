import { Fn } from '~/types'

/**
 * Enforces that a function is only callable one time.
 *
 * @param fn - The function.
 * @returns The one-time callable function.
 */
export function once(fn: Fn<void>): Fn<void> {
  let run = false

  return (...args) => {
    if (!run) {
      fn(...args)
      run = true
    }
  }
}
