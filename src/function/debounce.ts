import { Fn } from '~/types'

/**
 * Prevents function execution until it hasn't been called for a specified time period.
 *
 * @param fn - The function.
 * @param delay - The time period (in ms).
 * @returns The debounced function.
 */
export function debounce(fn: Fn<void>, delay: number): Fn<void> {
  if (delay < 0) {
    console.warn('[tsu] warning: debounce delays less than 0 will be set to 0.')
  }

  let timeout: ReturnType<typeof setTimeout>

  return function (...args) {
    clearTimeout(timeout)

    timeout = setTimeout(() => {
      clearTimeout(timeout)
      fn(...args)
    }, delay)
  }
}
