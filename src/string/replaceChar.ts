/**
 * Replaces a character at a specified index in a string with a new character.
 *
 * @param str - The string.
 * @param i - The index of the character to replace.
 * @param char - The character to replace with.
 * @returns The modified string with the character replaced.
 */
export function replaceChar(str: string, i: number, char: string): string {
  if (i < 0 || i > str.length - 1) return str

  return str.slice(0, i) + char + str.slice(i + 1)
}
