/**
 * Separates a string into an array of characters.
 *
 * @param str - The string.
 * @returns The string's characters.
 */
export function chars(str: string): string[] {
  return str.split('')
}
