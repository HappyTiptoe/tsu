/**
 * Capitalises the first letter of one word, or all words, in a string.
 *
 * @param str - The string.
 * @param allWords - If all words should be capitalised.
 * @param delimiter - The delimiter to split the string by.
 * @returns The capitalised string.
 */
export function capitalise(str: string, allWords = false, delimiter = ' '): string {
  if (allWords) {
    return str
      .split(delimiter)
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(delimiter)
  }

  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const capitalize = capitalise
