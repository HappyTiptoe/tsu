/**
 * Converts a camel case string to kebab case.
 *
 * @param str - The camel case string.
 * @returns The kebab case string.
 */
export function toKebab(str: string): string {
  const RE = /\B([A-Z])/g
  return str.replace(RE, '-$1').toLowerCase()
}
