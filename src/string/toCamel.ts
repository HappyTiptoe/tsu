/**
 * Converts a kebab case string to camel case.
 *
 * @param str - The kebab case string.
 * @returns The camel case string.
 */
export function toCamel(str: string): string {
  const RE = /-(\w)/g
  return str.replace(RE, (_, c) => (c ? c.toUpperCase() : ''))
}
