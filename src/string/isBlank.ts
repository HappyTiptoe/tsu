/**
 * Identifies strings which only contain whitespace.
 *
 * @param str - The string.
 * @returns If the string is blank.
 */
export function isBlank(str: string): boolean {
  return str.trim().length === 0
}
