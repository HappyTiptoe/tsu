/**
 * Truncates a string to a provided length.
 *
 * @param str - The string.
 * @param length - The length.
 * @param suffix - The suffix to apply after truncation.
 * @returns The truncated string.
 */
export function truncate(str: string, length: number, suffix = '...'): string {
  if (length <= 0) {
    return suffix
  }

  return str.slice(0, length) + suffix
}
