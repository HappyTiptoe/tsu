const NUMERALS = {
  1: 'I',
  4: 'IV',
  5: 'V',
  9: 'IX',
  10: 'X',
  40: 'XL',
  50: 'L',
  90: 'XC',
  100: 'C',
  400: 'CD',
  500: 'D',
  900: 'CM',
  1000: 'M'
}

/**
 * Converts a given number to Roman numerals.
 *
 * @param n - The number to convert to Roman numerals.
 * @param lowerCase - If the numerals should be in lowercase.
 * @returns The Roman numeral representation of the given number.
 */
export function toRomanNumerals(n: number, lowerCase = false): string {
  let nCopy = n
  let result = ''

  const keys = Object.keys(NUMERALS)
    .map(Number)
    .sort((a, b) => b - a)

  for (const key of keys) {
    while (nCopy >= key) {
      result += NUMERALS[key as keyof typeof NUMERALS]
      nCopy -= key
    }
  }

  return lowerCase ? result.toLowerCase() : result
}
