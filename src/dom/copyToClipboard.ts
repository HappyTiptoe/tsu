import { isBrowser } from '~/is'

function legacyCopy(value: string) {
  const el = document.createElement('textarea')

  el.value = value ?? ''
  el.style.position = 'absolute'
  el.style.opacity = '0'

  document.body.appendChild(el)

  el.select()

  document.execCommand('copy')

  el.remove()
}

/**
 * Copies the provided string to the user's clipboard.
 *
 * @param str - The string to be copied to the clipboard.
 */
export function copyToClipboard(str: string) {
  if (!isBrowser()) return

  if (window.navigator && 'clipboard' in window.navigator) {
    navigator.clipboard.writeText(str)
  } else {
    legacyCopy(str)
  }
}
