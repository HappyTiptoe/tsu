import { isNumber, isString, isWindow } from '~/is'
import { clamp } from '~/number'

interface ScrollConfig {
  to: Element | string | number
  offset?: number
  duration?: number
  container?: Element | string | null
}

function easing(t: number) {
  return t < 0.5 ? 4 * Math.pow(t, 3) : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
}

function getPosition(s: number, e: number, t: number, d: number) {
  return s + (e - s) * easing(t / d)
}

function getEnd(target: number | Element, start: number, container: Element | Window) {
  let end: number

  if (isNumber(target)) {
    end = target
  } else if (isWindow(container)) {
    end = target.getBoundingClientRect().top + start
  } else {
    // account for scroll container's offset from page top
    end = target.getBoundingClientRect().top + start - container.getBoundingClientRect().top
  }

  return end
}

/**
 * Scrolls the page or provided container to a target element or y-coordinate.
 *
 * @param config - The scroll config.
 * @param config.to - The scroll target.
 * @param config.offset - The offset from the scroll target (in px).
 * @param config.duration - The scroll duration (in ms).
 * @param config.container - The container to scroll in.
 */
export function scroll({ to, offset = 0, duration = 1000, container = null }: ScrollConfig): void {
  const target = isString(to) ? document.querySelector<HTMLElement>(to)! : to
  const _container = isString(container)
    ? document.querySelector<HTMLElement>(container)!
    : container || window

  const start = isWindow(_container)
    ? window.pageYOffset || document.documentElement.scrollTop
    : _container.scrollTop || 0

  const maxScroll = isWindow(_container)
    ? document.documentElement.scrollHeight - window.innerHeight
    : _container.scrollHeight - _container.clientHeight

  const unclampedEnd = getEnd(target, start, _container) + offset
  const end = clamp(unclampedEnd, 0, maxScroll)

  const startTime = Date.now()

  function step() {
    const timeElapsed = Date.now() - startTime
    const isScrolling = timeElapsed < duration
    const position = isScrolling ? getPosition(start, end, timeElapsed, duration) : end

    if (isScrolling) {
      requestAnimationFrame(step)
    }

    if (isWindow(_container)) {
      // Use scrollTo for Window object
      window.scrollTo(0, position)
    } else if (_container instanceof HTMLElement) {
      // Use scrollTop for HTML elements
      _container.scrollTop = position
    }
  }

  step()
}
