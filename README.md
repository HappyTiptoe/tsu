<div align="center">
  <a href="https://gitlab.com/happytiptoe/tsu">
    <img src="https://gitlab.com/happytiptoe/tsu/-/raw/main/assets/logo.png" alt="tsu - opinionated project scaffolder" width="251">
  <br>
  </a>
  <h1>tsu</h1>
  <h3>✨ TypeScript Utilities ✨</h3>
</div>

<div align="center">
<a href="https://www.npmjs.com/package/tsu" target="__blank">
  <img src="https://img.shields.io/npm/v/tsu?color=3eaa47&label=" alt="NPM version">
</a>
<a href="https://tsu.pages.dev" target="__blank">
  <img src="https://img.shields.io/static/v1?label=&message=View%20documentation&color=3eaa47" alt="view documentation">
</a>
<br>
<a href="https://gitlab.com/happytiptoe/tsu" target="__blank">
  <img src="https://img.shields.io/gitlab/stars/happytiptoe/tsu?style=flat&color=131514" alt="gitlab stars">
</a>
<a href="https://npmjs.com/package/tsu">
  <img src="https://img.shields.io/npm/l/tsu?color=131514" alt="license">
</a>
</div>

## Installation

Install with your favourite package manager:

```sh
# yarn
yarn add tsu

# pnpm
pnpm add tsu

# npm
npm install tsu
```

Import as required:

```js
import { ... } from 'tsu'
```

## Documentation

Full documentation is available [here](https://tsu.pages.dev/).

## Credits

- [antfu](https://github.com/antfu) - For the inspiration to make a utils library.
- [yuanqing](https://github.com/yuanqing) - For the package name.

## License

[MIT](http://opensource.org/licenses/MIT)
