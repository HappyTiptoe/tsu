import { describe, expect, test } from 'vitest'
import * as O from '../src/object'

describe('object:clone', () => {
  test("should return the input if it's not an array or an object", () => {
    const input = 1
    const cloned = O.clone(1)

    expect(input).toEqual(cloned)
  })

  test('should clone a simple array', () => {
    const array = [1, 2, 3]
    const cloned = O.clone(array)

    expect(cloned).not.toBe(array)
    expect(cloned).toEqual(array)
  })

  test('should clone a simple object', () => {
    const object = { foo: 'bar' }
    const cloned = O.clone(object)

    expect(cloned).not.toBe(object)
    expect(cloned).toEqual(object)
  })

  test('should clone a complicated array', () => {
    type A = [number, string, number[], { foo: string }]

    const array: A = [1, 'a', [1, 2], { foo: 'bar' }]
    const cloned = O.clone(array)

    expect(cloned).not.toBe(array)
    expect(cloned).toEqual(array)

    cloned[3].foo = 'baz'

    expect(array[3].foo).toBe('bar')
  })

  test('should clone a complicated object', () => {
    const object = { foo: 'bar', baz: { qux: 42, quux: [1, 2, 3, 4, 5] } }
    const cloned = O.clone(object)

    expect(cloned).not.toBe(object)
    expect(cloned).toEqual(object)

    expect(cloned.baz).not.toBe(object.baz)
    expect(cloned.baz).toEqual(object.baz)

    cloned.foo = 'test'
    expect(object.foo).toBe('bar')

    cloned.baz.quux[0] = 100
    expect(object.baz.quux[0]).toBe(1)
  })
})

describe('object:omit', () => {
  test('should return a copy of an object, without specified keys', () => {
    const input = { a: 1, b: 2, c: 3, d: 4 }
    const omitted = O.omit(input, ['c', 'd'])

    expect(omitted).toEqual({ a: 1, b: 2 })
  })

  test('should return an empty object if all keys are provided', () => {
    const input = { a: 1, b: 2, c: 3, d: 4 }
    const omitted = O.omit(input, ['a', 'b', 'c', 'd'])

    expect(omitted).toEqual({})
  })

  test('should return a copy of an object if no keys are provided', () => {
    const input = { a: 1, b: 2, c: 3, d: 4 }
    const omitted = O.omit(input, [])

    expect(omitted).toEqual(input)
  })

  test("should return a copy of an object, if the provided keys don't exist", () => {
    const input = { a: 1, b: 2, c: 3, d: 4 }
    const omitted = O.omit(input, ['e'])

    expect(omitted).toEqual(input)
  })
})
