import { describe, expect, test } from 'vitest'
import * as I from '../src/is'

const fn = (x: any) => x
const obj = { test: 'test' }

const items = [true, 1, -1, '-1', 'test', [], [1, 2, 3], obj, {}, fn, undefined, null]

describe('is: isArray', () => {
  test('should identify array values', () => {
    const arrays = items.filter(I.isArray)
    expect(arrays).toEqual([[], [1, 2, 3]])
  })
})

describe('is: isBoolean', () => {
  test('should identify boolean values', () => {
    const booleans = items.filter(I.isBoolean)
    expect(booleans).toEqual([true])
  })
})

describe('is: isDefined', () => {
  test('should identify defined values', () => {
    const defineds = JSON.stringify(items.filter(I.isDefined))
    expect(defineds).not.toContain('undefined')
  })
})

describe('is: isEmptyArray', () => {
  test('should identify empty arrays', () => {
    const emptyArrays = items.filter(I.isEmptyArray)
    expect(emptyArrays).toEqual([[]])
  })
})

describe('is: isEmptyObject', () => {
  test('should identify empty objects', () => {
    const emptyObjects = items.filter(I.isEmptyObject)
    expect(emptyObjects).toEqual([{}])
  })
})

describe('is: isFunction', () => {
  test('should identify functions', () => {
    const functions = items.filter(I.isFunction)
    expect(functions).toEqual([fn])
  })
})

describe('is: isNumber', () => {
  test('should identify numeric values', () => {
    const numbers = items.filter(I.isNumber)
    expect(numbers).toEqual([1, -1])
  })
})

describe('is: isNull', () => {
  test('should identify nullish values', () => {
    const nulls = items.filter(I.isNull)
    expect(nulls).toEqual([null])
  })
})

describe('is: isObject', () => {
  test('should identify objects', () => {
    const objects = items.filter(I.isObject)
    expect(objects).toEqual([obj, {}])
  })
})

describe('is: isObjectLike', () => {
  test('should identify object-like values', () => {
    const objectLikes = items.filter(I.isObjectLike)
    expect(objectLikes).toEqual([[], [1, 2, 3], obj, {}, null])
  })
})

describe('is: isString', () => {
  test('should identify strings', () => {
    const strings = items.filter(I.isString)
    expect(strings).toEqual(['-1', 'test'])
  })
})

describe('is: isUndefined', () => {
  test('should identify undefined values', () => {
    const undefineds = items.filter(I.isUndefined)
    expect(undefineds).toEqual([undefined])
  })
})
