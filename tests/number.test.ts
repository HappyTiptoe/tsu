import { describe, expect, test } from 'vitest'
import * as N from '../src/number'

const numbers = [-10, -5, -3, -2, -1, 0, 1, 2, 3, 5, 10]

describe('number: average', () => {
  test('should calculate the average of a number array', () => {
    const ns = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    const avg = N.average(ns)

    expect(avg).toBe(5.5)
  })

  test('should calculate an average of 0 if provided an empty array', () => {
    const ns: number[] = []
    const avg = N.average(ns)

    expect(avg).toBe(0)
  })
})

describe('number: ceil', () => {
  test('should round a number up to the given factor', () => {
    const n1 = N.ceil(1.2, 3)
    expect(n1).toBe(3)

    const n2 = N.ceil(-9.8, 7)
    expect(n2).toBe(-7)

    const n3 = N.ceil(0, 5)
    expect(n3).toBe(0)
  })

  test('should round a number up to the nearest 1 if no factor is provided', () => {
    const n1 = N.ceil(1.2)
    expect(n1).toBe(2)

    const n2 = N.ceil(-9.8)
    expect(n2).toBe(-9)

    const n3 = N.ceil(0)
    expect(n3).toBe(0)
  })
})

describe('number: clamp', () => {
  test('should set a value lower than the minimum bound to the minimum bound', () => {
    const n1 = N.clamp(-1, 0, 1)
    expect(n1).toBe(0)

    const n2 = N.clamp(1, 10, 20)
    expect(n2).toBe(10)
  })

  test('should set a value higher than the maximum bound to the maximum bound', () => {
    const n1 = N.clamp(2, 0, 1)
    expect(n1).toBe(1)

    const n2 = N.clamp(21, 10, 20)
    expect(n2).toBe(20)
  })

  test('should not modify a value in between the specified bounds', () => {
    const n1 = N.clamp(0.5, 0, 1)
    expect(n1).toBe(0.5)

    const n2 = N.clamp(15, 10, 20)
    expect(n2).toBe(15)
  })
})

describe('number: floor', () => {
  test('should round a number down to the given factor', () => {
    const n1 = N.floor(1.2, 3)
    expect(n1).toBe(0)

    const n2 = N.floor(-9.8, 7)
    expect(n2).toBe(-14)

    const n3 = N.floor(0, 5)
    expect(n3).toBe(0)
  })

  test('should round a number down to the nearest 1 if no factor is provided', () => {
    const n1 = N.floor(1.2)
    expect(n1).toBe(1)

    const n2 = N.floor(-9.8)
    expect(n2).toBe(-10)

    const n3 = N.floor(0)
    expect(n3).toBe(0)
  })
})

describe('number: isEven', () => {
  test('should identify even numbers', () => {
    const evens = numbers.filter(N.isEven)
    expect(evens).toEqual([-10, -2, 0, 2, 10])
  })
})

describe('number: isOdd', () => {
  test('should identify odd numbers', () => {
    const odds = numbers.filter(N.isOdd)
    expect(odds).toEqual([-5, -3, -1, 1, 3, 5])
  })
})

describe('number: max', () => {
  test('should return -Infinity if provided no arguments', () => {
    const max = N.max()

    expect(max).toBe(-Infinity)
  })

  test('should return -Infinity if provided an empty array', () => {
    const array: [] = []
    const max = N.max(array)

    expect(max).toBe(-Infinity)
  })

  test('should return the maximum value of provided numbers', () => {
    const max = N.max(1, 2, 3, 4, 5)

    expect(max).toBe(5)
  })

  test('should return the maximum value in an array', () => {
    const array = [1, 2, 3, 4, 5]
    const max = N.max(array)

    expect(max).toBe(5)
  })
})

describe('number: min', () => {
  test('should return Infinity if provided no arguments', () => {
    const max = N.min()

    expect(max).toBe(Infinity)
  })

  test('should return Infinity if provided an empty array', () => {
    const array: [] = []
    const max = N.min(array)

    expect(max).toBe(Infinity)
  })

  test('should return the minimum value of provided numbers', () => {
    const max = N.min(1, 2, 3, 4, 5)

    expect(max).toBe(1)
  })

  test('should return the minimum value in an array', () => {
    const array = [1, 2, 3, 4, 5]
    const max = N.min(array)

    expect(max).toBe(1)
  })
})

describe('number: product', () => {
  test('should return 0 if provided an empty array', () => {
    const array: [] = []
    const product = N.product(array)

    expect(product).toBe(0)
  })

  test('should return the product of an array', () => {
    const array = [1, 2, 3, 4, 5]
    const product = N.product(array)

    expect(product).toBe(120)
  })

  test('should return the product of a negative array', () => {
    const array = [-1, -2, -3, -4, -5]
    const product = N.product(array)

    expect(product).toBe(-120)
  })
})

describe('number: round', () => {
  test('should round a number to the given factor', () => {
    const n1 = N.round(1.2, 3)
    expect(n1).toBe(0)

    const n2 = N.round(-9.8, 7)
    expect(n2).toBe(-7)

    const n3 = N.round(0, 5)
    expect(n3).toBe(0)
  })

  test('should round a number to the nearest 1 if no factor is provided', () => {
    const n1 = N.round(1.2)
    expect(n1).toBe(1)

    const n2 = N.round(-9.8)
    expect(n2).toBe(-10)

    const n3 = N.round(0)
    expect(n3).toBe(0)
  })
})

describe('number: sum', () => {
  test('should return 0 if provided an empty array', () => {
    const array: [] = []
    const sum = N.sum(array)

    expect(sum).toBe(0)
  })

  test('should return the sum of an array', () => {
    const array = [1, 2, 3, 4, 5]
    const sum = N.sum(array)

    expect(sum).toBe(15)
  })

  test('should return the sum of a negative array', () => {
    const array = [-1, -2, -3, -4, -5]
    const sum = N.sum(array)

    expect(sum).toBe(-15)
  })
})

describe('number: toDp', () => {
  test('should round a number to the specified number of decimal places', () => {
    const n = 1.23456789

    const to3Dp = N.toDp(n, 3)
    expect(to3Dp).toBe(1.235)

    const to5Dp = N.toDp(n, 5)
    expect(to5Dp).toBe(1.23457)

    const to20Dp = N.toDp(n, 20)
    expect(to20Dp).toBe(1.23456789)
  })

  test('should round a number to 0 decimal places if none specified', () => {
    const n = 1.23456789
    const toDp = N.toDp(n)

    expect(toDp).toBe(1)
  })
})

describe('number: toNumber', () => {
  test('should return NaN if provided an empty string', () => {
    const string = ''
    const number = N.toNumber(string)

    expect(number).toBe(NaN)
  })

  test('should remove common symbols to form a valid number', () => {
    const string = '#£1,234$%,567€89'
    const number = N.toNumber(string)

    expect(number).toBe(123456789)
  })

  test('should remove custom symbols to form a valid number', () => {
    const string = '123z456'
    const number = N.toNumber(string, ['z'])

    expect(number).toBe(123456)

    const string2 = '123@:456'
    const number2 = N.toNumber(string2, ['@', ':'])

    expect(number2).toBe(123456)
  })
})
