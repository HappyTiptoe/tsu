import { describe, expect, test } from 'vitest'
import * as S from '../src/string'

describe('string: capitalise', () => {
  test('should return an empty string if provided an empty string', () => {
    const string = ''
    const capitalisedString = S.capitalise(string)

    expect(capitalisedString).toBe('')
  })

  test('should capitalise only the first word of a string', () => {
    const string = 'the quick brown fox jumps over the lazy dog.'
    const capitalisedString = S.capitalise(string)

    expect(capitalisedString).toBe('The quick brown fox jumps over the lazy dog.')
  })

  test('should capitalise the words in a string', () => {
    const string = 'the quick brown fox jumps over the lazy dog.'
    const capitalisedString = S.capitalise(string, true)

    expect(capitalisedString).toBe('The Quick Brown Fox Jumps Over The Lazy Dog.')
  })

  test('should capitalise the words in a string with respect to a delimiter', () => {
    const string = 'the/quick/brown/fox/jumps/over/the/lazy/dog.'
    const capitalisedString = S.capitalise(string, true, '/')

    expect(capitalisedString).toBe('The/Quick/Brown/Fox/Jumps/Over/The/Lazy/Dog.')
  })
})

describe('string: chars', () => {
  test('should return an empty array if provided an empty string', () => {
    const string = ''
    const chars = S.chars(string)

    expect(chars).toEqual([])
  })

  test('should split a string into its characters', () => {
    const string = 'hello world'
    const chars = S.chars(string)

    // prettier-ignore
    expect(chars).toEqual(['h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'])
  })
})

describe('string: isBlank', () => {
  test('should identify empty strings as blank', () => {
    const string = ''
    const isBlank = S.isBlank(string)

    expect(isBlank).toBe(true)
  })

  test('should identify strings containing whitespace as blank', () => {
    const string = '  '
    const isBlank = S.isBlank(string)

    expect(isBlank).toBe(true)
  })

  test('should not identify strings containing characters as blank', () => {
    const string = 'hello world'
    const isBlank = S.isBlank(string)

    expect(isBlank).toBe(false)
  })
})

describe('string: isEmpty', () => {
  test('should identify empty strings as empty', () => {
    const string = ''
    const isEmpty = S.isEmpty(string)

    expect(isEmpty).toBe(true)
  })

  test('should not identify strings containing characters as empty', () => {
    const string = 'hello world'
    const isEmpty = S.isEmpty(string)

    expect(isEmpty).toBe(false)
  })
})

describe('string: replaceChar', () => {
  test('should replace a specified index with a specified character', () => {
    const string = 'hello world'
    const replaced = S.replaceChar(string, 1, 'o')

    expect(replaced).toBe('hollo world')
  })

  test('should not modify the string if the index is < 0', () => {
    const string = 'hello world'
    const replaced = S.replaceChar(string, -1, 'o')

    expect(replaced).toBe('hello world')
  })

  test('should not modify the string if the index is > 0', () => {
    const string = 'hello world'
    const replaced = S.replaceChar(string, 11, 'o')

    expect(replaced).toBe('hello world')
  })
})

describe('string: splitString', () => {
  test('should return the two parts of the string, either side of a specified position', () => {
    const string = 'test'
    const result = S.splitString(string, 2)

    expect(result).toEqual(['te', 'st'])
  })

  test('should return two empty strings if provided an empty string', () => {
    const result = S.splitString('', 1)

    expect(result).toEqual(['', ''])
  })

  test('should return an empty string and the whole string if the split index is < 0', () => {
    const string = 'test'
    const result = S.splitString(string, -1)

    expect(result).toEqual(['', 'test'])
  })

  test('should return an empty string and the whole string if the split index equals 0', () => {
    const string = 'test'
    const result = S.splitString(string, 0)

    expect(result).toEqual(['', 'test'])
  })

  test('should return the whole string and an empty string if the split index exceeds the length of the string', () => {
    const string = 'test'
    const result = S.splitString(string, 100)

    expect(result).toEqual(['test', ''])
  })
})

describe('string: toCamel', () => {
  test('should return an empty string if provided an empty string', () => {
    const string = ''
    const camelString = S.toCamel(string)

    expect(camelString).toBe('')
  })

  test('should replace hyphen delimiters with capital letter delimiters', () => {
    const string = 'testing-testing-testing123'
    const camelString = S.toCamel(string)

    expect(camelString).toBe('testingTestingTesting123')
  })
})

describe('string: toKebab', () => {
  test('should return an empty string if provided an empty string', () => {
    const string = ''
    const kebabString = S.toKebab(string)

    expect(kebabString).toBe('')
  })

  test('should replace capital letter delimiters with hyphen delimiters', () => {
    const string = 'testingTestingTesting123'
    const kebabString = S.toKebab(string)

    expect(kebabString).toBe('testing-testing-testing123')
  })
})

describe('string: toOrdinal', () => {
  test('should apply the correct ordinal suffix to numbers', () => {
    const numbers = [1, 2, 3, 4, 5]
    const ordinalNumbers = numbers.map(S.toOrdinal)

    expect(ordinalNumbers).toEqual(['1st', '2nd', '3rd', '4th', '5th'])
  })

  test('should apply the correct ordinal suffix to negative numbers', () => {
    const numbers = [-1, -2, -3, -4, -5]
    const ordinalNumbers = numbers.map(S.toOrdinal)

    expect(ordinalNumbers).toEqual(['-1st', '-2nd', '-3rd', '-4th', '-5th'])
  })
})

describe('string: toRomanNumerals', () => {
  test('should convert a number to Roman numerals', () => {
    const n1 = S.toRomanNumerals(1)
    expect(n1).toBe('I')

    const n2 = S.toRomanNumerals(19)
    expect(n2).toBe('XIX')

    const n3 = S.toRomanNumerals(1999)
    expect(n3).toBe('MCMXCIX')
  })

  test('should convert a number to lowercase Roman numerals, if specified', () => {
    const n1 = S.toRomanNumerals(1999, true)
    expect(n1).toBe('mcmxcix')
  })
})

describe('string: truncate', () => {
  test('should truncate a string', () => {
    const string = 'hello world'
    const truncatedStr = S.truncate(string, 9)

    expect(truncatedStr).toBe('hello wor...')
  })

  test('should truncate a string and add a suffix', () => {
    const string = 'hello world'
    const truncatedStr = S.truncate(string, 3, '-')

    expect(truncatedStr).toBe('hel-')
  })

  test('should return the truncateStr for an empty string and any length', () => {
    const string = ''
    const truncatedStr = S.truncate(string, 1)

    expect(truncatedStr).toBe('...')
  })

  test('should return the truncateStr for a length < 1', () => {
    const string = 'hello world'
    const truncatedStr = S.truncate(string, -10)

    expect(truncatedStr).toBe('...')
  })
})
