# isOdd

> Identifies if a number is odd.

## Usage

```ts
import { isOdd } from 'tsu'

isOdd(1)
// true

isOdd(2)
// false
```

## Type Definitions

```ts
/**
 * @param n - The number.
 * @returns If the number is odd.
 */
function isOdd(n: number): boolean
```
