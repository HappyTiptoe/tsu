# round

> Rounds a number to the nearest multiple of the specified factor.

## Usage

```ts
import { round } from 'tsu'

round(0.75)
// 1

round(8, 10)
// 10

round(2, 10)
// 0
```

## Type Definitions

```ts
/**
 * @param n - The number to round.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
function round(n: number, factor: number = 1): number
```
