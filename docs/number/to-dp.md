# toDp

> Rounds a number to a specified number of decimal places.

## Usage

:::warning
`toDp` will remove any trailing 0s. For example, `toDp(1.2, 5)` will yield `1.2`, not `1.20000`.
:::

```ts
import { toDp } from 'tsu'

toDp(1.23456789, 2)
// 1.23

toDp(1.23456789, 5)
// 1.23457

toDp(1.23456789)
// 1
```

## Type Definitions

```ts
/**
 * @param n - The number to round.
 * @param decimalPlaces - The number of decimal places to round to.
 * @returns The rounded number.
 */
function toDp(n: number, decimalPlaces = 0): number
```
