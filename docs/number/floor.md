# floor

> Rounds a number down to the nearest multiple of the specified factor.

## Usage

```ts
import { floor } from 'tsu'

floor(0.75)
// 0

floor(8, 10)
// 0

floor(2, 10)
// 0
```

## Type Definitions

```ts
/**
 * @param n - The number to round down.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
function floor(n: number, factor: number = 1): number
```
