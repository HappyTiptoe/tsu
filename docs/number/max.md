# max

> Returns the maximum number of provided numbers or array of numbers.

## Usage

```ts
import { max } from 'tsu'

max(1, 2, 3, 4, 5)
// 5

max([1, 2, 3, 4, 5])
// 5

max()
// -Infinity

max([])
// -Infinity
```

## Type Definitions

```ts
/**
 * @param ns - The numbers or the array of numbers.
 * @returns The maximum number.
 */
function max(...ns: number[] | [number[]]): number
```
