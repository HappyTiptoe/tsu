# clamp

> Restricts a number between two bounds.

## Usage

```ts
import { clamp } from 'tsu'

clamp(-100, 0, 10)
// 0

clamp(100, 0, 10)
// 10

clamp(5, 0, 10)
// 5
```

## Type Definitions

```ts
/**
 * @param n - The number to restrict.
 * @param min - The minimum bound.
 * @param max - The maximum bound.
 * @returns The number restricted between the specified bounds.
 */
function clamp(n: number, min: number, max: number): number
```
