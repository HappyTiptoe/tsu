# sum

> Returns the sum of a number array.

## Usage

```ts
import { sum } from 'tsu'

sum([1, 2, 3, 4, 5])
// 15

sum([])
// 0
```

## Type Definitions

```ts
/**
 * @param ns - The number array.
 * @returns The sum.
 */
function sum(ns: readonly number[]): number
```
