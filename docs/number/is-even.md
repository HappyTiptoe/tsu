# isEven

> Identifies if a number is even.

## Usage

```ts
import { isEven } from 'tsu'

isEven(1)
// false

isEven(2)
// true
```

## Type Definitions

```ts
/**
 * @param n - The number.
 * @returns If the number is even.
 */
function isEven(n: number): boolean
```
