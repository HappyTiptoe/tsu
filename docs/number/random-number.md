# randomNumber

> Generates a random integer between bounds.

## Usage

```ts
import { randomNumber } from 'tsu'

randomNumber(5)
// 0, 1, 2, 3, or 4

randomNumber(2, 5)
// 2, 3, or 4
```

## Type Definitions

```ts
/**
 * @param min - The inclusive minimum bound.
 * @param max - The exclusive maximum bound.
 * @returns The random integer.
 */
function randomNumber(max: number): number
function randomNumber(min: number, max: number): number
function randomNumber(...args: number[]): number
```
