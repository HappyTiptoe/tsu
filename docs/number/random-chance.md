# randomChance

> Rolls an n-sided die.

## Usage

```ts
import { randomChance } from 'tsu'

randomChance(10)
// returns true 1 in 10 times
```

## Type Definitions

```ts
/**
 * @param n - The number of sides on the die.
 * @returns If the die roll was 0.
 */
function randomChance(n: number): boolean
```
