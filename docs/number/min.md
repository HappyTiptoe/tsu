# min

> Returns the minimum number of provided numbers or array of numbers.

## Usage

```ts
import { min } from 'tsu'

min(1, 2, 3, 4, 5)
// 1

min([1, 2, 3, 4, 5])
// 1

min()
// Infinity

min([])
// Infinity
```

## Type Definitions

```ts
/**
 * @param ns - The numbers or the array of numbers.
 * @returns The minimum number.
 */
function min(...ns: number[] | [number[]]): number
```
