# ceil

> Rounds a number up to the nearest multiple of the specified factor.

## Usage

```ts
import { ceil } from 'tsu'

ceil(0.75)
// 1

ceil(8, 10)
// 10

ceil(2, 10)
// 10
```

## Type Definitions

```ts
/**
 * @param n - The number to round up.
 * @param factor - The factor used for rounding.
 * @returns The rounded number.
 */
function ceil(n: number, factor: number = 1): number
```
