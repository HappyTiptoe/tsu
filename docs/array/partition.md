# partition

> Splits an array into two based on a predicate function.

## Usage

```ts
import { partition } from 'tsu'

partition([1, 2, 3, 4, 5], (item) => n < 3)
// [[1, 2], [3, 4, 5]]
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @param filter - The predicate function.
 * @returns The tuple of values that satisfy the predicate and those that don't.
 */
function partition<T, U = T>(
  array: readonly (T | U)[],
  filter: (current: T | U, idx: number, array: readonly (T | U)[]) => boolean
): [T[], U[]]
```
