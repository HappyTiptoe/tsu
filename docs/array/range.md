# range

> Generates a range of numbers.

## Usage

```ts
import { range } from 'tsu'

range(5)
// [0, 1, 2, 3, 4]

range(10, 15)
// [10, 11, 12, 13, 14]

range(50, 100, 10)
// [50, 60, 70, 80, 90]
```

## Type Definitions

```ts
/**
 * @param start - The inclusive start value.
 * @param stop - The exlusive stop value.
 * @param step - The step to increment by.
 * @returns The range of numbers.
 */
function range(stop: number): number[]
function range(start: number, stop: number, step?: number): number[]
function range(...args: any[]): number[]
```
