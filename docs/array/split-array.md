# splitArray

> Splits an array into two at a specified index.

## Usage

```ts
import { splitArray } from 'tsu'

splitArray(['a', 'b', 'c', 'd', 'e'], 4)
// [['a', 'b', 'c', 'd'], ['e']]
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @param i - The position to split at.
 * @returns The tuple of values before and after the split.
 */
function splitArray<T>(array: readonly T[], i: number): [T[], T[]]
```
