# last

> Returns the last item of an array.

## Usage

```ts
import { last } from 'tsu'

last(['a', 'b', 'c', 'd', 'e'])
// 'e'
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @returns The last item.
 */
function last(array: readonly []): undefined
function last<T>(array: readonly T[]): T
function last<T>(array: readonly T[] | []): T | undefined
```
