# drop

> Removes the first `n` items of an array.

## Usage

```ts
import { drop } from 'tsu'

drop(['a', 'b', 'c', 'd', 'e'], 3)
// ['d', 'e']
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @param n - The number of items to remove.
 * @returns The remaining items.
 */
function drop<T>(array: readonly T[], n: number): T[]
```
