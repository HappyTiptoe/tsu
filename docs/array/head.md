# head

> Returns the first item of an array.

## Usage

```ts
import { head } from 'tsu'

head(['a', 'b', 'c', 'd', 'e'])
// 'a'
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @returns The first item.
 */
function head(array: readonly []): undefined
function head<T>(array: readonly T[]): T
function head<T>(array: readonly T[] | []): T | undefined
```
