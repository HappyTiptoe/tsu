# init

> Returns all but the last item of an array.

## Usage

```ts
import { init } from 'tsu'

init(['a', 'b', 'c', 'd', 'e'])
// ['a', 'b', 'c', 'd']
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @returns The array without the last item.
 */
function init<T>(array: readonly T[]): T[]
```
