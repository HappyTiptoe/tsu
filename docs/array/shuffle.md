# shuffle

> Randomly shuffles items in an array.

## Usage

```ts
import { shuffle } from 'tsu'

const shuffled = shuffle([0, 1, 2, 3, 4])
// eg: [3, 1, 0, 4, 2]
```

## Type Definitions

```ts
function shuffle<T>(array: readonly T[]): T[]
```
