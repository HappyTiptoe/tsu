# take

> Returns the first `n` items of an array.

## Usage

```ts
import { take } from 'tsu'

take(['a', 'b', 'c', 'd', 'e'], 3)
// ['a', 'b', 'c']
```

## Type Definitions

```ts
/**
 * @param array - The array.
 * @param n - The number of items to return.
 * @returns The first `n` items.
 */
function take<T>(array: readonly T[], n: number): T[]
```
