import { defineConfig } from 'vitepress'

export default defineConfig({
  appearance: true,
  description: 'A collection of TypeScript utilities.',
  head: [],
  lang: 'en-US',
  // lastUpdated: true,
  markdown: {
    theme: 'nord',
    lineNumbers: false
  },
  title: 'tsu',

  themeConfig: {
    logo: '/logo.png',
    search: { provider: 'local' },
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2021-Present HappyTiptoe'
    },
    nav: nav(),
    sidebar: sidebar()
  }
})

function nav() {
  return [
    { text: 'Get Started', link: '/get-started' },
    { text: 'Source', link: 'https://gitlab.com/HappyTiptoe/tsu' }
  ]
}

function sidebar() {
  return [
    { text: 'Guide', items: [{ text: 'Get Started', link: '/get-started' }] },
    {
      text: 'Array',
      collapsed: false,
      items: [
        { text: 'drop', link: '/array/drop' },
        { text: 'groups', link: '/array/groups' },
        { text: 'head', link: '/array/head' },
        { text: 'init', link: '/array/init' },
        { text: 'last', link: '/array/last' },
        { text: 'partition', link: '/array/partition' },
        { text: 'range', link: '/array/range' },
        { text: 'shuffle', link: '/array/shuffle' },
        { text: 'splitArray', link: '/array/split-array' },
        { text: 'tail', link: '/array/tail' },
        { text: 'take', link: '/array/take' },
        { text: 'toArray', link: '/array/to-array' },
        { text: 'uniq', link: '/array/uniq' }
      ]
    },
    {
      text: 'DOM',
      collapsed: false,
      items: [
        { text: 'copyToClipboard', link: '/dom/copy-to-clipboard' },
        { text: 'scroll', link: '/dom/scroll' }
      ]
    },
    {
      text: 'Function',
      collapsed: false,
      items: [
        { text: 'debounce', link: '/function/debounce' },
        { text: 'memoise', link: '/function/memoise' },
        { text: 'once', link: '/function/once' },
        { text: 'repeat', link: '/function/repeat' },
        { text: 'throttle', link: '/function/throttle' }
      ]
    },
    {
      text: 'Is',
      collapsed: false,
      items: [
        { text: 'isArray', link: '/is/is-array' },
        { text: 'isBoolean', link: '/is/is-boolean' },
        { text: 'isBrowser', link: '/is/is-browser' },
        { text: 'isDefined', link: '/is/is-defined' },
        { text: 'isEmptyArray', link: '/is/is-empty-array' },
        { text: 'isEmptyObject', link: '/is/is-empty-object' },
        { text: 'isFunction', link: '/is/is-function' },
        { text: 'isNull', link: '/is/is-null' },
        { text: 'isNumber', link: '/is/is-number' },
        { text: 'isObject', link: '/is/is-object' },
        { text: 'isObjectLike', link: '/is/is-object-like' },
        { text: 'isString', link: '/is/is-string' },
        { text: 'isTouchDevice', link: '/is/is-touch-device' },
        { text: 'isUndefined', link: '/is/is-undefined' },
        { text: 'isWindow', link: '/is/is-window' }
      ]
    },
    {
      text: 'Miscellaneous',
      collapsed: false,
      items: [
        { text: 'noop', link: '/misc/noop' },
        { text: 'sleep', link: '/misc/sleep' },
        { text: 'uuid', link: '/misc/uuid' }
      ]
    },
    {
      text: 'Number',
      collapsed: false,
      items: [
        { text: 'average', link: '/number/average' },
        { text: 'ceil', link: '/number/ceil' },
        { text: 'clamp', link: '/number/clamp' },
        { text: 'floor', link: '/number/floor' },
        { text: 'isEven', link: '/number/is-even' },
        { text: 'isOdd', link: '/number/is-odd' },
        { text: 'max', link: '/number/max' },
        { text: 'min', link: '/number/min' },
        { text: 'product', link: '/number/product' },
        { text: 'randomChance', link: '/number/random-chance' },
        { text: 'randomNumber', link: '/number/random-number' },
        { text: 'round', link: '/number/round' },
        { text: 'sum', link: '/number/sum' },
        { text: 'toDp', link: '/number/to-dp' },
        { text: 'toNumber', link: '/number/to-number' }
      ]
    },
    {
      text: 'Object',
      collapsed: false,
      items: [
        { text: 'clone', link: '/object/clone' },
        { text: 'es', link: '/object/es' },
        { text: 'ks', link: '/object/ks' },
        { text: 'omit', link: '/object/omit' },
        { text: 'vs', link: '/object/vs' }
      ]
    },
    {
      text: 'String',
      collapsed: false,
      items: [
        { text: 'capitalise', link: '/string/capitalise' },
        { text: 'chars', link: '/string/chars' },
        { text: 'isBlank', link: '/string/is-blank' },
        { text: 'isEmpty', link: '/string/is-empty' },
        { text: 'replaceChar', link: '/string/replace-char' },
        { text: 'splitString', link: '/string/split-string' },
        { text: 'toCamel', link: '/string/to-camel' },
        { text: 'toKebab', link: '/string/to-kebab' },
        { text: 'toOrdinal', link: '/string/to-ordinal' },
        { text: 'toRomanNumerals', link: '/string/to-roman-numerals' },
        { text: 'truncate', link: '/string/truncate' },
        { text: 'unchars', link: '/string/unchars' }
      ]
    }
  ]
}
