---
layout: home
title: Home

hero:
  name: 🪷 tsu
  text: A collection of TypeScript utilities
  actions:
    - theme: brand
      text: Get Started
      link: /get-started
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/HappyTiptoe/tsu
---
