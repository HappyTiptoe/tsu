# splitString

> Splits a string into two at a specified index.

## Usage

```ts
import { splitString } from 'tsu'

splitString('ribbit', 2)
// ['ri', 'bbit']
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @param i - The position to split at.
 * @returns The tuple of strings before and after the split.
 */
function splitString(str: string, i: number): [string, string]
```
