# truncate

> Truncates a string to a provided length.

## Usage

```ts
import { truncate } from 'tsu'

truncate('ribbit ribbit', 9)
// 'ribbit ri...'

truncate('ribbit', 5, '-')
// 'ribbi-'
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @param length - The length.
 * @param suffix - The suffix to apply after truncation.
 * @returns The truncated string.
 */
function truncate(str: string, length: number, suffix: string = '...'): string
```
