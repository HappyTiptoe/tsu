# toRomanNumerals

> Converts a given number to Roman numerals.

## Usage

```ts
import { toRomanNumerals } from 'tsu'

toRomanNumerals(1234)
// 'MCCXXXIV'

toRomanNumerals(1234, true)
// 'mccxxxiv'
```

## Type Definitions

```ts
/**
 * @param n - The number to convert to Roman numerals.
 * @param lowerCase - If the numerals should be in lowercase.
 * @returns The Roman numeral representation of the given number.
 */
function toRomanNumerals(n: number, lowerCase = false): string
```
