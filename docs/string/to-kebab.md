# toKebab

> Converts a camel case string to kebab case.

## Usage

```ts
import { toKebab } from 'tsu'

toKebab('ribbitRibbit')
// 'ribbit-ribbit'
```

## Type Definitions

```ts
/**
 * @param str - The camel case string.
 * @returns The kebab case string.
 */
function toKebab(str: string): string
```
