# toCamel

> Converts a kebab case string to camel case.

## Usage

```ts
import { toCamel } from 'tsu'

toCamel('ribbit-ribbit')
// 'ribbitRibbit'
```

## Type Definitions

```ts
/**
 * @param str - The kebab case string.
 * @returns The camel case string.
 */
function toCamel(str: string): string
```
