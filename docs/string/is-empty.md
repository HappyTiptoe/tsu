# isEmpty

> Identifies empty strings.

## Usage

```ts
import { isEmpty } from 'tsu'

isEmpty('ribbit')
// false

isEmpty('   ')
// false

isEmpty('')
// true
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @returns If the string is empty.
 */
function isEmpty(str: string): boolean
```
