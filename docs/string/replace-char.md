# replaceChar

> Replaces a character at a specified index in a string with a new character.

## Usage

```ts
import { replaceChar } from 'tsu'

replaceChar('ribbit', 1, 'a')
// 'rabbit'
```

## Type Definitions

```ts
/**
 * @param str - The string.
 * @param i - The index of the character to replace.
 * @param char - The character to replace with.
 * @returns The modified string with the character replaced.
 */
function replaceChar(str: string, i: number, char: string): string
```
