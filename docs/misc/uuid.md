# uuid

> Generates a random string of a specified length.

## Usage

```ts
import { uuid } from 'tsu'

uuid()
// random 16-character string

uuid(8)
// random 8-character string
```

## Type Definitions

```ts
/**
 * @param length - The length of the randomly generated string.
 * @returns The randomly generated string.
 */
function uuid(length: number = 16): string
```
