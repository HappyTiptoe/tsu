# noop

> Does nothing.

## Usage

```ts
import { noop } from 'tsu'

noop()
// nothing
```

## Type Definitions

```ts
function noop(): void
```
