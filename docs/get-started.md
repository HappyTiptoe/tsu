# Get Started

**tsu** is a collection of [TypeScript](https://www.typescriptlang.org/docs/) utility functions. They can also be used in JavaScript projects as well!

## Installation

Install with your favourite package manager:

```sh
# yarn
yarn add tsu

# pnpm
pnpm add tsu

# npm
npm install tsu
```

Import as required:

```ts
// ESM
import { ... } from 'tsu'
```

```ts
// CommonJS
const { ... } = require('tsu')
```
