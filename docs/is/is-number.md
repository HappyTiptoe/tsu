# isNumber

> Identifies if a value is a number.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is a number.
 */
function isNumber(val: unknown): val is number
```
