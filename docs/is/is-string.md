# isString

> Identifies if a value is a string.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is a string.
 */
function isString(val: unknown): val is string
```
