# isNull

> Identifies if a value is null.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is null.
 */
function isNull(val: unknown): val is null
```
