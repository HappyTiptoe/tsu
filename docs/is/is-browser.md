# isBrowser

> Identifies if the code is being run in a browser.

## Type Definitions

```ts
/**
 * @returns If the code is being run in a browser.
 */
function isBrowser(): boolean
```
