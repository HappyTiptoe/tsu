# isUndefined

> Identifies if a value is undefined.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is undefined.
 */
function isUndefined(val: unknown): val is undefined
```
