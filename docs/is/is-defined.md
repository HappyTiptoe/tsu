# isDefined

> Identifies if a value is defined.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is defined.
 */
function isDefined<T>(val: T): val is Exclude<T, null | undefined>
```
