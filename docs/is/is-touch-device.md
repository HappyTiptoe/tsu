# isTouchDevice

> Identifies if the code is being run on a touch device.

## Type Definitions

```ts
/**
 * @returns If the code is being run on a touch device.
 */
function isTouchDevice(): boolean
```
