# isFunction

> Identifies if a value is a function.

## Type Definitions

```ts
/**
 * @param val - The value.
 * @returns If the value is a function.
 */
function isFunction<T extends Fn>(val: unknown): val is T
```
