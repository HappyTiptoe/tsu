# scroll

> Scrolls the page or provided container to a target element or y-coordinate.

## Usage

```ts
import { scroll } from 'tsu'

scroll({
  to: '#target',
  offset: 0,
  duration: 250,
  container: '#scroll-container'
})
```

## Type Definitions

```ts
interface ScrollConfig {
  to: Element | string | number
  offset?: number
  duration?: number
  container?: Element | string | null
}

/**
 * @param config - The scroll config.
 * @param config.to - The scroll target.
 * @param config.offset - The offset from the scroll target (in px).
 * @param config.duration - The scroll duration (in ms).
 * @param config.container - The container to scroll in.
 */
function scroll({ to, offset = 0, duration = 1000, container = null }: ScrollConfig): void
```
