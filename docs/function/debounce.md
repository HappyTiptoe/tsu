# debounce

> Prevents function execution until it hasn't been called for a specified time period.

## Usage

```ts
import { debounce } from 'tsu'

const ribbit = debounce(() => {
  console.log('ribbit')
}, 1000)
// logs when `ribbit` hasn't been called for 1000ms
```

## Type Definitions

```ts
/**
 * @param fn - The function.
 * @param delay - The time period (in ms).
 * @returns The debounced function.
 */
function debounce(fn: (...args: any[]) => void, delay: number): (...args: any[]) => void
```
