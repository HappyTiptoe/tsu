# repeat

> Executes a given function a specified number of times.

## Usage

```ts
import { repeat } from 'tsu'

repeat(() => {
  console.log('ribbit')
}, 3)
// logs `ribbit` to the console 3 times
```

## Type Definitions

```ts
/**
 * @param fn - The function to execute for each iteration.
 * @param n - The number of times to execute the function.
 */
function repeat(fn: (i: number) => void, n: number): void
```
