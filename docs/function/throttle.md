# throttle

> Prevents function execution for a specified time period after it was last called.

## Usage

```ts
import { throttle } from 'tsu'

const ribbit = throttle(() => {
  console.log('ribbit')
}, 1000)
// can only log once per 1000ms
```

## Type Definitions

```ts
/**
 * @param fn - The function.
 * @param limit - The time period (in ms).
 * @returns The throttled function.
 */
function throttle(fn: (...args: any[]) => void, limit: number): (...args: any[]) => void
```
