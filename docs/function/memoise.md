# memoise

> Optimises subsequent calls of a function by caching return values.

:::tip NOTE
**tsu** also exports this function as `memoize`
:::

## Usage

```ts
import { memoise } from 'tsu'

const add = memoise((x: number, y: number) => {
  return x + y
})

add(1, 2)
// calculates 3

add(1, 2)
// obtains 3 from cache
```

## Type Definitions

```ts
/**
 * @param fn - The function.
 * @returns The memoised function.
 */
function memoise<T>(fn: (...args: any[]) => T): (...args: any[]) => T
```
