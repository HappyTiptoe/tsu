# omit

> Creates a shallow copy of an object, omitting the specified keys.

## Usage

```ts
import { omit } from 'tsu'

const object = { foo: 'bar', baz: 'quux' }
const noFoo = omit(object, ['foo'])
// noFoo: { baz: 'quux' }
```

## Type Definitions

```ts
/**
 * @param object - The object to copy.
 * @param keysToOmit - The keys to omit from the copied object.
 * @returns A shallow copy of the object, without the specified keys.
 */
function omit<T extends Record<string, any>, K extends keyof T>(
  object: T,
  keysToOmit: K[] | any[]
): Pick<T, Exclude<keyof T, K>>
```
