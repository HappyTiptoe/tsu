# es

> Strict-typed, shorthand `Object.entries`

## Usage

```ts
import { es } from 'tsu'

const obj = { foo: 'bar', baz: 42 }

es(obj)
// [['foo', 'bar'], ['baz', 42]]
```

## Type Definitions

```ts
/**
 * @param obj - The object to extract the entries from.
 * @returns The entries.
 */
function es<T extends object>(obj: T): [keyof T, T[keyof T]][]
```
