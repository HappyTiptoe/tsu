# clone

> Deep clones an array or object.

## Usage

```ts
import { clone } from 'tsu'

const array = [1, 2, 3]
const clonedArray = clone(array)
// clonedArray: [1, 2, 3]

array === clonedArray
// false

const object = { foo: 'bar' }
const clonedObject = clone(object)
// clonedObject: { foo: 'bar' }

object === clonedObject
// false
```

## Type Definitions

```ts
/**
 * @param input - The array or object to clone.
 * @returns A deep clone of the input array or object.
 */
function clone<T>(input: T): T
```
