# ks

> Strict-typed, shorthand `Object.keys`

## Usage

```ts
import { ks } from 'tsu'

const obj = { foo: 'bar', baz: 42 }

ks(obj)
// ['foo', 'baz']
```

## Type Definitions

```ts
/**
 * @param obj - The object to extract the keys from.
 * @returns The keys.
 */
function ks<T extends object>(
  obj: T
): Array<`${keyof T & (string | number | boolean | null | undefined)}`>
```
